///
/// main datatype we use to manipulate pixels in pixdraw.
///
const std = @import("std");
const print = std.debug.print;
const assert = std.debug.assert;
const expect = std.testing.expect;

const Pixel = u32;
const Allocator = std.mem.Allocator;

const Self = @This();

allocator: Allocator,
width: u32,
height: u32,
pixels: []Pixel,
bg_col: Pixel,

pub fn init(allocator: Allocator, width: u32, height: u32, pixels: []Pixel, bg_col: Pixel) Self {
    return Self{
        .allocator = allocator,
        .width = width,
        .height = height,
        .pixels = pixels,
        .bg_col = bg_col,
    };
}

pub fn deinit(self: Self) void {
    self.allocator.free(self.pixels);
}

pub fn fillBgCol(self: Self) void {
    for (self.pixels, 0..) |_, i| {
        self.pixels[i] = self.bg_col;
    }
}

pub fn fill(self: Self, color: Pixel) void {
    for (self.pixels, 0..) |_, i| {
        self.pixels[i] = color;
    }
}

pub fn clone(self: Self) !Self {
    var pxs = try self.allocator.alloc(Pixel, self.width * self.height);
    for (self.pixels, 0..) |_, i| {
        pxs[i] = self.pixels[i];
    }
    return Self{
        .allocator = self.allocator,
        .width = self.width,
        .height = self.height,
        .pixels = pxs,
        .bg_col = self.bg_col,
    };
}

pub fn copyTo(self: Self, other: Self) void {
    for (self.pixels, 0..) |_, i| {
        other.pixels[i] = self.pixels[i];
    }
}

pub fn saveAsPpm(self: Self, filename: []const u8) !void {
    const file = try std.fs.cwd().createFile(
        filename,
        .{ .read = true },
    );
    defer file.close();

    const writer = file.writer();
    var buf_writer = std.io.bufferedWriter(writer);

    var buffer: [50]u8 = undefined;
    const ppm_header = try std.fmt.bufPrint(buffer[0..], "P6\n{d} {d} 255\n", .{ self.width, self.height });
    _ = try buf_writer.write(ppm_header);

    var i: usize = 0;
    while (i < self.width * self.height) : (i += 1) {
        const pixel = self.pixels[i];
        const bytes = [3]u8{
            @intCast((pixel >> 8 * 2) & 0xFF), // get Red value
            @intCast((pixel >> 8 * 1) & 0xFF), // get Green value
            @intCast((pixel >> 8 * 0) & 0xFF), // get Blue value
        };
        _ = try buf_writer.write(&bytes);
    }
    _ = try buf_writer.flush();
    print("### Picture saved as {s} ###\n", .{filename});
}

pub fn flip(self: Self) !void {
    var tmp_pic = try self.allocator.alloc(Pixel, self.pixels.len);
    defer self.allocator.free(tmp_pic);
    for (self.pixels, 0..) |px, i| {
        tmp_pic[i] = px;
    }

    var j: u32 = 0;
    while (j < self.height) : (j += 1) {
        var i: u32 = 0;
        while (i < self.width) : (i += 1) {
            const new_j = j;
            const new_i = self.width - 1 - i;
            self.pixels[new_i + new_j * self.width] = tmp_pic[i + j * self.width];
        }
    }
}

pub fn flop(self: Self) !void {
    var tmp_pic = try self.allocator.alloc(Pixel, self.pixels.len);
    defer self.allocator.free(tmp_pic);
    for (self.pixels, 0..) |px, i| {
        tmp_pic[i] = px;
    }

    var j: u32 = 0;
    while (j < self.height) : (j += 1) {
        var i: u32 = 0;
        while (i < self.width) : (i += 1) {
            const new_j = self.height - 1 - j;
            const new_i = i;
            self.pixels[new_i + new_j * self.width] = tmp_pic[i + j * self.width];
        }
    }
}

// shift functions

/// ~~~~ SHIFT FUNCTIONS ~~~~
/// get number of column where index is
pub fn getColNb(self: Self, index: usize) usize {
    return index % self.width;
}

/// get row number according to index
pub fn getRowNb(self: Self, index: usize) usize {
    return index / self.width;
}

fn colorColumn(self: Self, colnb: u32, color: Pixel) void {
    assert(colnb < self.width);

    var x = colnb;
    while (x < self.pixels.len) : (x += self.width) {
        self.pixels[x] = color;
    }
}

fn colorRow(self: Self, rownb: u32, color: Pixel) void {
    assert(rownb < self.height);

    const x = rownb * self.width;
    var i: u32 = 0;
    while (i < self.width) : (i += 1) {
        self.pixels[x + i] = color;
    }
}

/// shift whole `self` delta px to the right
pub fn shiftRight(self: Self, delta: u16) !void {
    const tmp = try self.allocator.alloc(Pixel, self.pixels.len);
    defer self.allocator.free(tmp);

    for (self.pixels, 0..) |_, i| {
        tmp[i] = self.pixels[i];
    }

    for (tmp, 0..) |_, i| {
        if (self.getRowNb(i + delta) == self.getRowNb(i)) {
            self.pixels[i + delta] = tmp[i];
        }
    }

    var k: u32 = 0;
    while (k < delta) : (k += 1) {
        colorColumn(self, k, self.bg_col);
    }
    // ^ add delta column of bg pixels to the left of canvas
}

pub fn shiftLeft(self: Self, delta: u16) !void {
    const tmp = try self.allocator.alloc(Pixel, self.pixels.len);
    defer self.allocator.free(tmp);

    for (self.pixels, 0..) |_, i| {
        tmp[i] = self.pixels[i];
    }

    for (tmp, 0..) |_, i| {
        if (i >= delta and (self.getRowNb(i - delta) == self.getRowNb(i))) {
            self.pixels[i - delta] = tmp[i];
        }
    }
    // ^ move all pixels delta rows to the right

    var k: u32 = self.width - delta;
    while (k < self.width) : (k += 1) {
        colorColumn(self, k, self.bg_col);
    }
    // ^ add delta rown of pixels on the right of canvas
}

pub fn shiftUp(self: Self, delta: u32) !void {
    const tmp = try self.allocator.alloc(Pixel, self.pixels.len);
    defer self.allocator.free(tmp);

    for (self.pixels, 0..) |_, i| {
        tmp[i] = self.pixels[i];
    }

    for (tmp, 0..) |_, i| {
        if (i >= self.width * delta) {
            self.pixels[i - self.width * delta] = tmp[i];
        }
    }

    var k: u32 = self.height - delta;
    while (k < self.height) : (k += 1) {
        colorRow(self, k, self.bg_col);
    }

    // ^ add delta rows of pixels at the bottom of self
}

pub fn shiftDown(self: Self, delta: u32) !void {
    const tmp = try self.allocator.alloc(Pixel, self.pixels.len);
    defer self.allocator.free(tmp);

    for (self.pixels, 0..) |_, i| {
        tmp[i] = self.pixels[i];
    }

    for (tmp, 0..) |_, i| {
        if (i + delta * self.width < self.pixels.len) {
            self.pixels[i + delta * self.width] = tmp[i];
        }
    }

    var k: u32 = 0;
    while (k < delta) : (k += 1) {
        colorRow(self, k, self.bg_col);
    }
    // ^ add delta rows of bg on top of self
}

//
//
//
//
//
//

pub fn drawRect(self: Self, x: i32, y: i32, w: i32, h: i32, color: Pixel) void {
    const casted_width: i32 = @intCast(self.width);
    const casted_height: i32 = @intCast(self.height);

    var j: i32 = if (y >= 0) y else 0;
    while (j < y + h) : (j += 1) {
        if (j < casted_height) {
            var i: i32 = if (x >= 0) x else 0;
            if (i < casted_width) {
                while (i < x + w) : (i += 1) {
                    const pixel_index: usize = @intCast(i + j * casted_width);
                    self.pixels[pixel_index] = color;
                }
            }
        }
    }
}

pub fn drawCircle(self: Self, cx: i32, cy: i32, radius: i32, color: Pixel) void {
    const casted_width: i32 = @intCast(self.width);
    const casted_height: i32 = @intCast(self.height);

    const xstart = cx - radius;
    const ystart = cy - radius;

    const xend = cx + radius;
    const yend = cy + radius;

    var y: i32 = if (ystart >= 0) ystart else 0;
    while (y <= yend) : (y += 1) {
        if (y < casted_height) {
            var x: i32 = if (xstart >= 0) xstart else 0;
            while (x <= xend) : (x += 1) {
                if (x < casted_width) {
                    const square_dist = (cx - x) * (cx - x) + (cy - y) * (cy - y);

                    if (square_dist <= (radius * radius)) {
                        const casted_x: u64 = @intCast(x);
                        const casted_y: u64 = @intCast(y);
                        self.pixels[@intCast(casted_y * self.width + casted_x)] = color;
                    }
                }
            }
        }
    }
}

fn absVal(x: i32) i32 {
    return (if (x > 0) x else -x);
}

fn getLineLength(self: Self, x0: i32, y0: i32, x1: i32, y1: i32) i32 {
    _ = self;
    const f_x0: f32 = @floatFromInt(x0);
    const f_y0: f32 = @floatFromInt(y0);

    const f_x1: f32 = @floatFromInt(x1);
    const f_y1: f32 = @floatFromInt(y1);

    return (@intFromFloat(@sqrt((f_x1 - f_x0) * (f_x1 - f_x0) + (f_y1 - f_y0) * (f_y1 - f_y0))));
}

/// triangle drawing algorithm fromµ : http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html#sunbresenhamarticle
fn fillBottomFlat(self: Self, x0: i32, y0: i32, x1: i32, y1: i32, x2: i32, y2: i32, brush_size: u32, color: Pixel) void {
    const f_x0: f32 = @floatFromInt(x0);
    const f_y0: f32 = @floatFromInt(y0);

    const f_x1: f32 = @floatFromInt(x1);
    const f_y1: f32 = @floatFromInt(y1);

    const f_x2: f32 = @floatFromInt(x2);
    const f_y2: f32 = @floatFromInt(y2);

    const invslope1: f32 = (f_x1 - f_x0) / (f_y1 - f_y0);
    const invslope2: f32 = (f_x2 - f_x0) / (f_y2 - f_y0);

    var curx0: f32 = f_x0;
    var curx1: f32 = f_x0;

    var scanlineY: f32 = f_y0;

    while (scanlineY <= f_y1) : (scanlineY += 1) {
        drawLine(self, @intFromFloat(curx0), @intFromFloat(scanlineY), @intFromFloat(curx1), @intFromFloat(scanlineY), brush_size, color);
        curx0 += invslope1;
        curx1 += invslope2;
    }
}

fn fillTopFlat(self: Self, x0: i32, y0: i32, x1: i32, y1: i32, x2: i32, y2: i32, brush_size: u32, color: Pixel) void {
    const f_x0: f32 = @floatFromInt(x0);
    const f_y0: f32 = @floatFromInt(y0);

    const f_x1: f32 = @floatFromInt(x1);
    const f_y1: f32 = @floatFromInt(y1);

    const f_x2: f32 = @floatFromInt(x2);
    const f_y2: f32 = @floatFromInt(y2);

    const invslope1: f32 = (f_x2 - f_x0) / (f_y2 - f_y0);
    const invslope2: f32 = (f_x2 - f_x1) / (f_y2 - f_y1);

    var curx0: f32 = f_x2;
    var curx1: f32 = f_x2;

    var scanlineY: f32 = f_y2;

    while (scanlineY > f_y0) : (scanlineY -= 1) {
        drawLine(self, @intFromFloat(curx0), @intFromFloat(scanlineY), @intFromFloat(curx1), @intFromFloat(scanlineY), brush_size, color);
        curx0 -= invslope1;
        curx1 -= invslope2;
    }
}

fn swap(x0: *i32, y0: *i32, x1: *i32, y1: *i32) void {
    const tmpx0 = x0.*;
    const tmpy0 = y0.*;

    x0.* = x1.*;
    y0.* = y1.*;

    x1.* = tmpx0;
    y1.* = tmpy0;
}








pub fn drawTriangle(self: Self, x0: i32, y0: i32, x1: i32, y1: i32, x2: i32, y2: i32, brush_size: u32, color: Pixel) void {

    // sort vertices so that topmost is lower y
    var true_x0: i32 = x0;
    var true_y0: i32 = y0;

    var true_x1: i32 = x1;
    var true_y1: i32 = y1;

    var true_x2: i32 = x2;
    var true_y2: i32 = y2;

    if (true_y0 > true_y1) swap(&true_x0, &true_y0, &true_x1, &true_y1);
    if (true_y1 > true_y2) swap(&true_x1, &true_y1, &true_x2, &true_y2);
    if (true_y0 > true_y1) swap(&true_x0, &true_y0, &true_x1, &true_y1);

    //

    if (true_y1 == true_y2) {
        fillBottomFlat(self, true_x0, true_y0, true_x1, true_y1, true_x2, true_y2, brush_size, color);
    } else if (true_y0 == true_y1) {
        fillTopFlat(self, true_x0, true_y0, true_x1, true_y1, true_x2, true_y2, brush_size, color);
    } else {
        const f_x0: f32 = @floatFromInt(true_x0);
        const f_y0: f32 = @floatFromInt(true_y0);

        const f_y1: f32 = @floatFromInt(true_y1);

        const f_x2: f32 = @floatFromInt(true_x2);
        const f_y2: f32 = @floatFromInt(true_y2);

        const x4: i32 = @intFromFloat(f_x0 + ((f_y1 - f_y0) / (f_y2 - f_y0)) * (f_x2 - f_x0));
        const y4 = true_y1;

        fillBottomFlat(self, true_x0, true_y0, true_x1, true_y1, x4, y4, brush_size, color);
        fillTopFlat(self, true_x1, true_y1, x4, y4, true_x2, true_y2, brush_size, color);
    }
}

pub fn drawQuad(self: Self, x0: i32, y0: i32, x1: i32, y1: i32, x2: i32, y2: i32, x3: i32, y3: i32, brush_size: u32 ,color: Pixel) void {
    // sort vertices so that topmost is lower y
    var true_x0: i32 = x0;
    var true_y0: i32 = y0;

    var true_x1: i32 = x1;
    var true_y1: i32 = y1;

    var true_x2: i32 = x2;
    var true_y2: i32 = y2;

    var true_x3: i32 = x3;
    var true_y3: i32 = y3;

    if (true_y0 > true_y1) swap(&true_x0, &true_y0, &true_x1, &true_y1);
    if (true_y2 > true_y3) swap(&true_x2, &true_y2, &true_x3, &true_y3);
    if (true_y0 > true_y2) swap(&true_x0, &true_y0, &true_x2, &true_y2);
    if (true_y1 > true_y3) swap(&true_x1, &true_y1, &true_x3, &true_y3);
    if (true_y1 > true_y2) swap(&true_x1, &true_y1, &true_x2, &true_y2);
    print("quad : {d} {d} {d} {d} {d} {d} {d} {d}\n", .{true_x0, true_y0, true_x1, true_y1, true_x2, true_y2, true_x3, true_y3});

    drawTriangle(self, true_x0, true_y0, true_x1, true_y1, true_x2, true_y2, brush_size, color);
    drawTriangle(self, true_x1, true_y1, true_x2, true_y2, true_x3, true_y3, brush_size, color);
}


/// from wikipédia : x0 + (1-t)^2*(x0 - x0) + t2*(x0-x0) <=> (1-t)^2*x0 + 2*t*x0*(1-t) + t^2*x0
/// https://en.wikipedia.org/wiki/B%C3%A9zier_curve
/// WIP, broken
pub fn drawBezier(self: Self, x0: i32, y0: i32, x1: i32, y1: i32, x2: i32, y2: i32, brush_size: u32, color: Pixel) void {
    const f_x0: f32 = @floatFromInt(x0);
    const f_y0: f32 = @floatFromInt(y0);

    const f_x1: f32 = @floatFromInt(x1);
    const f_y1: f32 = @floatFromInt(y1);

    const f_x2: f32 = @floatFromInt(x2);
    const f_y2: f32 = @floatFromInt(y2);

    var old_x: i32 = x0;
    var old_y: i32 = y0;

    var t: f32 = 0.0;
    // FIXME : float comparison ?
    while (t<=1.01) : (t+=0.1) {
        const x: f32 = ((1-t)*(1-t)) * f_x0 + 2*t*f_x1*(1-t) + (t*t)*f_x2;
        const y: f32 = ((1-t)*(1-t)) * f_y0 + 2*t*f_y1*(1-t) + (t*t)*f_y2;
        const int_x: i32 = @intFromFloat(x);
        const int_y: i32 = @intFromFloat(y);

        drawLine(self, old_x, old_y, int_x, int_y, brush_size, color); 
        old_x = int_x;
        old_y = int_y;
    }
}









/// all pixels with same color as old_color will be changed to new_color
pub fn paintAllSameColorPixels(self: Self, old_color: Pixel, new_color: Pixel) void {
    for (self.pixels, 0..) |px, i| {
        if (px == old_color) {
            self.pixels[i] = new_color;
        }
    }
}

/// paint area around point (x, y)
/// FIXME : pixel overflow / pixel wrapping on the sides
pub fn brushPaint(self: Self, x0: i32, y0: i32, brush_size: u32, color: Pixel) void {
    if (brush_size == 1) {
        const casted_x0: u64 = @intCast(x0);
        const casted_y0: u64 = @intCast(y0);

        self.pixels[@intCast(casted_x0 + casted_y0 * @as(u64, self.width))] = color;
        return;
    }

    const dt: i32 = @intCast(brush_size / 2);
    var y: i32 = y0 - dt;
    while (y < y0 + dt) : (y += 1) {
        if (y >= 0 and y < self.height) {
            var x: i32 = x0 - dt;
            while (x < x0 + dt) : (x += 1) {
                if (x >= 0 and x < self.width) {
                    const casted_x: u64 = @intCast(x);
                    const casted_y: u64 = @intCast(y);
                    const index: usize = @intCast(casted_x + casted_y * @as(u64, self.width));
                    self.pixels[index] = color;
                }
            }
        }
    }
}

pub fn drawLineNaive(self: Self, x0: i32, y0: i32, x1: i32, y1: i32, brush_size: u32, color: Pixel) void {
    const dx: i32 = x1 - x0;
    const dy: i32 = y1 - y0;

    if (dx > 0) {
        var x: i32 = x0;
        while (x <= x1) : (x += 1) {
            const y = @divFloor(dy * (x - x0), dx) + y0;
            brushPaint(self, x, y, brush_size, color);
        }
    }
}

/// Bresenham algorithm as described on this link :
/// http://members.chello.at/~easyfilter/bresenham.html
pub fn drawLineBresenham(self: Self, x0: i32, y0: i32, x1: i32, y1: i32, brush_size: u32, color: Pixel) void {
    const dx = absVal(x1 - x0);
    const sx: i32 = if (x0 < x1) 1 else -1;

    const dy = -(absVal(y1 - y0));
    const sy: i32 = if (y0 < y1) 1 else -1;

    var er = dx + dy;
    var e2: i32 = undefined;

    var x = x0;
    var y = y0;
    while (true) {
        brushPaint(self, x, y, brush_size, color);
        if (x == x1 and y == y1) break;
        e2 = 2 * er;

        if (e2 >= dy) {
            er += dy;
            x += sx;
        }

        if (e2 <= dx) {
            er += dx;
            y += sy;
        }
    }
}

pub const drawLine = drawLineBresenham;

/// color all directly adjacent pixels
pub fn bucketFill(self: Self, x: i32, y: i32, color: Pixel) !void {
    var stack = std.ArrayList(u32).init(self.allocator);
    defer stack.deinit();

    const casted_x: u64 = @intCast(x);
    const casted_y: u64 = @intCast(y);
    const index: u64 = @intCast(casted_x + casted_y * @as(u64, self.width));
    const color_of_first_pixel = self.pixels[index];

    try stack.append(@intCast(index));

    while (stack.items.len > 0) {
        const index1 = stack.pop(); //NOTE : POP() REMOVES THE LAST ELEMENT FROM THE STACK NOT THE FIRST!!!!
        if (self.pixels[index1] == color_of_first_pixel and self.pixels[index1] != color) {
            self.pixels[index1] = color;

            if (index1 + 1 < self.width * self.height and (self.getRowNb(index1) == self.getRowNb(index1 + 1))) {
                try stack.append(index1 + 1);
            }
            if (index1 + self.width < self.width * self.height) {
                try stack.append(index1 + self.width);
            }
            if (index1 >= 1 and (self.getRowNb(index1) == self.getRowNb(index1 - 1))) {
                try stack.append(index1 - 1);
            }
            if (index1 >= self.width) {
                try stack.append(index1 - self.width);
            }
        }
    }
}

pub fn reduce(self: Self, new_width: u32, new_height: u32, xoffset: u32, yoffset: u32) !Self {
    var pixels = try self.allocator.alloc(Pixel, new_width * new_height);

    var y = yoffset;
    while (y < new_height + yoffset) : (y += 1) {
        var x = xoffset;
        while (x < new_width + xoffset) : (x += 1) {
            pixels[(x - xoffset) + (y - yoffset) * new_width] = self.pixels[x + y * self.width];
        }
    }

    return Self{
        .allocator = self.allocator,
        .width = new_width,
        .height = new_height,
        .pixels = pixels,
        .bg_col = self.bg_col,
    };
}



/// nearest neighbor algorithm, as described here :
/// https://tech-algorithm.com/articles/nearest-neighbor-image-scaling/
pub fn scale(self: Self, new_width: u32, new_height: u32) !Self{
    const w1 = self.width;
    const h1 = self.height;

    const w2 = new_width;
    const h2 = new_height;

    var pixels = try self.allocator.alloc(Pixel, w2 * h2);
    const x_ratio = ((w1 << 16) / w2) + 1;
    const y_ratio = ((h1 << 16) / h2) + 1;

    assert(x_ratio != 0 and y_ratio != 0);

    var i: u32 = 0;
    while (i < h2) : (i += 1) {
        var j: u32 = 0;
        while (j < w2) : (j += 1) {
            const x1 = (j * x_ratio) >> 16;
            const y1 = (i * y_ratio) >> 16;
            pixels[i * w2 + j] = self.pixels[@intCast(y1 * w1 + x1)];
        }
    }

    return Self{
        .allocator = self.allocator,
        .width = new_width,
        .height = new_height,
        .pixels = pixels,
        .bg_col = self.bg_col,
    };
}






test {
    try expect(absVal(-50) == 50);
    try expect(absVal(50) == 50);
    try expect(absVal(-43 + 57) == 14);
    try expect(absVal(102 - 125) == 23);
    try expect(absVal(102 - (-125)) == 227);
}

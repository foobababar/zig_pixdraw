const std = @import("std");
const print = std.debug.print;
const Allocator = std.mem.Allocator;
const assert = std.debug.assert;
const absInt = std.math.absInt;

const Pixel = u32;

/// 1 Pixel is like 0xAARRGGBB
/// AA is for transparency (00 = transparent) (FF = max opacity)
const testing = std.testing;

const sdl = @import("c.zig");

const Picture = @import("Picture.zig");
const Colors = @import("Colors.zig");

/// ~~~~ ERRORS ~~~~
const Errors = error{
    FailedToInitializeSdl,
    FailedToCreateWindow,
    FailedToCreateRenderer,
    FailedToCreateTexture,
    IncorrectNumberOfArgs,
    CouldNotGetScreenSize,
    BadMagic,

    NotImplemented,
};

/// TMP PPM FUNCTIONS

pub fn argbToArgb(a: u8, r: u8, g: u8, b: u8) Pixel {
    return (@as(u32, a) * 256 * 256 * 256 + @as(u32, r) * 256 * 256 + @as(u32, g) * 256 + @as(u32, b));
}

/// open a ppm file then strip the header and return only the pixels
/// will put picture dimensions inside w and h
/// throw BadMagic error if there is a problem
pub fn getPixelsFromPpm(allocator: Allocator, ppm_file: []const u8, w: *u32, h: *u32) ![]Pixel {
    if (ppm_file[0] != 80 or ppm_file[1] != 54 or ppm_file[2] != '\n') {
        return Errors.BadMagic;
    }

    if (ppm_file.len < 11) {
        return Errors.BadMagic;
    }

    var it = std.mem.tokenize(u8, ppm_file, "\n");
    _ = it.next();

    const header = it.next().?;
    const index_start_picture = header.len + 4;
    // header.len+4 is first byte after header

    var it2 = std.mem.tokenize(u8, header, " ");

    if (std.fmt.parseInt(u32, it2.next().?, 10)) |captured| {
        w.* = captured;
    } else |_| {
        return Errors.BadMagic;
    }

    if (std.fmt.parseInt(u32, it2.next().?, 10)) |captured| {
        h.* = captured;
    } else |_| {
        return Errors.BadMagic;
    }

    var pixels: []Pixel = try allocator.alloc(Pixel, (w.*) * (h.*));

    for (pixels, 0..) |_, i| {
        if (i * 3 >= ppm_file.len or i * 3 + 1 >= ppm_file.len or i * 3 + 2 >= ppm_file.len) return Errors.BadMagic;
        pixels[i] = argbToArgb(255, ppm_file[index_start_picture..][i * 3], ppm_file[index_start_picture..][i * 3 + 1], ppm_file[index_start_picture..][i * 3 + 2]);
    }
    // ^ this for loop takes 3 bytes (with each iteration) from ppm_file and converts them to 1 pixel in the slice pixels
    // ^ 255 is for max opacity

    return pixels;
}

/// read ppm file into a slice
/// header is NOT stripped
pub fn readPpmFile(allocator: Allocator, filename: []const u8) ![]const u8 {
    // print("reading file : {s}\n", .{filename});
    const maxsize = 1 * 1024 * 1024;

    const file = try std.fs.cwd().openFile(filename, .{});
    defer file.close();

    const bytes: []const u8 = (try file.reader().readUntilDelimiterOrEofAlloc(allocator, '|', maxsize)).?;

    return bytes;
}

/// ~~~~ SDL, C, UTILITY FUNCTIONS ~~~~
fn loadConfigFile(allocator: Allocator, filename: []const u8) !struct { w: u32, h: u32 } {
    const maxsize = 1 * 1024 * 1024;

    var w1: u32 = undefined;
    var h1: u32 = undefined;

    const file = try std.fs.cwd().openFile(filename, .{});
    defer file.close();

    var file_content = std.ArrayList([]const u8).init(allocator);
    defer file_content.deinit();

    while (try file.reader().readUntilDelimiterOrEofAlloc(allocator, '\n', maxsize)) |line| {
        try file_content.append(line);
    }

    for (file_content.items) |item| {
        var it = std.mem.tokenize(u8, item, " ");

        while (it.next()) |tok| {
            if (std.mem.eql(u8, tok, "width")) {
                _ = it.next();
                w1 = try std.fmt.parseInt(u32, it.next().?, 10);
            } else if (std.mem.eql(u8, tok, "height")) {
                _ = it.next();
                h1 = try std.fmt.parseInt(u32, it.next().?, 10);
            }
        }
    }

    for (file_content.items) |item| {
        allocator.free(item);
    }

    return .{
        .w = w1,
        .h = h1,
    };
}

fn hidePointer() void {
    _ = sdl.SDL_ShowCursor(sdl.SDL_DISABLE);
}

fn showPointer() void {
    _ = sdl.SDL_ShowCursor(sdl.SDL_ENABLE);
}

fn getScreenDimensions() struct { w: u32, h: u32 } {
    const xratio = 5;
    const yratio = 3;
    const default_window_multiplicator = 6 * 40;

    const default_window_width = xratio * default_window_multiplicator;
    const default_window_height = yratio * default_window_multiplicator;

    var dm: sdl.SDL_DisplayMode = undefined;

    if (sdl.SDL_GetCurrentDisplayMode(0, &dm) != 0) {
        print("could not determine screen size, defaulting to {d} x {d} \n", .{ default_window_width, default_window_height });

        return .{
            .w = default_window_width,
            .h = default_window_height,
        };
    }

    print("found screen dimensions : {d} x {d} \n", .{ dm.w, dm.h });

    return .{
        .w = @intCast(dm.w),
        .h = @intCast(dm.h),
    };
}

/// call this function before resizing canvas
fn resizeRenderer(window: *sdl.SDL_Window, renderer: **sdl.SDL_Renderer, new_width: u32, new_height: u32) !void {
    sdl.SDL_DestroyRenderer(renderer.*);

    renderer.* = sdl.SDL_CreateRenderer(window, -1, 0) orelse return Errors.FailedToCreateRenderer;
    _ = sdl.SDL_SetRenderDrawBlendMode(renderer.*, sdl.SDL_BLENDMODE_BLEND);
    _ = sdl.SDL_RenderSetLogicalSize(renderer.*, @intCast(new_width), @intCast(new_height));
}

fn recreateTexture(renderer: *sdl.SDL_Renderer, texture: **sdl.SDL_Texture, new_width: u32, new_height: u32) !void {
    sdl.SDL_DestroyTexture(texture.*);
    texture.* = sdl.SDL_CreateTexture(renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, @intCast(new_width), @intCast(new_height)) orelse return Errors.FailedToCreateTexture;
    _ = sdl.SDL_SetTextureBlendMode(texture.*, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)
}

fn resizePicture(allocator: Allocator, renderer: *sdl.SDL_Renderer, texture: **sdl.SDL_Texture, canvas: *Picture, new_width: u32, new_height: u32) !void {
    try recreateTexture(renderer, texture, new_width, new_height);
    const tmp_color = canvas.*.bg_col;

    canvas.*.deinit();

    canvas.* = try Picture.init(allocator, new_width, new_height, try allocator.alloc(Pixel, new_width * new_height), tmp_color);
    canvas.*.fillBgCol();
}


/// ~~~~ LINE DRAWING FUNCTIONS ~~~~
fn showHelp() void {
    const help =
        \\
        \\ ~~~~ HELP ~~~~
        \\
        \\* run with : zig-out/bin/pixdraw 
        \\* open file : zig-out/bin/pixdraw mypicture.ppm
        \\* open multiple files : zig-out/bin/pixdraw px1.ppm px2.ppm
        \\
        \\
        \\ left click : paint
        \\ a : spawn palette
        \\ d : color picker
        \\ b : color all same color pixel mode as clicked px
        \\ f : fill shape mode
        \\
        \\ s : save current layer as a ppm file
        \\ ctrl + s : save all layers into a single ppm file 
        \\
        \\ l : enter text mode
        \\ p : create new layer
        \\ o : destroy top layer
        \\
        \\ t : click on 3 points to draw triangle
        \\ c : draw circles
        \\ e : click on 2 points to draw a line between them
        \\ n : normal paint mode
        \\
        \\ q/esc : exit brutally
        \\ 1/2 : increase/decrease brush size
        \\ 6 : display this help
        \\ ctrl + z : undo (max 20 times)
        \\ arrows : pan (when zoomed)
        \\
        \\ 7/8/9/0 : shift canvas left/down/up/right
        \\
        \\ m : mirror canvas (vertically)
        \\ y : mirror canvas horizontally
        \\
    ;
    print("{s}\n", .{help});
}






/// put canvas2 "on top of" canvas1
/// result is in canvas1
fn mergePicture(canvas1: Picture, canvas2: Picture) void {
    assert(canvas1.pixels.len == canvas2.pixels.len);

    for (canvas2.pixels, 0..) |pixel, i| {
        if (pixel != Colors.transparent) {
            canvas1.pixels[i] = pixel;
        }
    }
}


/// return an heap allocated Picture resulting of the superposition of all canvas in stack
fn mergePictureStack(allocator: Allocator, stack: std.ArrayList(Picture)) !Picture {
    const w = stack.items[0].width;
    const h = stack.items[0].height;
    const fusion = Picture.init(allocator, w, h, try allocator.alloc(Pixel, w * h), Colors.white.toHex());

    if (stack.items.len == 1) {
        mergePicture(fusion, stack.items[0]);
    } else {
        for (stack.items, 0..) |_, i| {
            mergePicture(fusion, stack.items[i]);
        }
    }
    return fusion;
}

/// generate black to white color palette
fn generatePalette(palette: Picture) void {
    const w = 200;
    const h = 120;
    var color = Colors.black;

    const nb_colors = 40;
    const cell_width = w / nb_colors;
    const cell_height = h / nb_colors;
    const increment = 255 / nb_colors;
    var colors_drawn: u8 = 0;

    var j: i32 = 0;
    while (j < palette.height) : (j += 1) {
        var i: i32 = 0;
        while (i < palette.width) : (i += 1) {
            palette.drawRect(i * cell_width, j * cell_height, cell_width, cell_height, color.toHex());
            colors_drawn += 1;

            if (colors_drawn == nb_colors) return;

            color.r += increment;
            color.g += increment;
            color.b += increment;
        }
    }
}

const Mode = enum(u8) {
    paint,
    circle,
    bucket,
    all_same_color,
    color_pick,
    draw_line,
    triangle,
    quad,
    palette,
    bezier,
};

pub fn main() !void {

    const fps = 60;
    const max_undos = 30;

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();

    //
    const init_status: i32 = sdl.SDL_Init(sdl.SDL_INIT_VIDEO);

    if (init_status != 0) {
        return Errors.FailedToInitializeSdl;
    }

    const screen_dimensions = getScreenDimensions(); // <- this line must be after SDL_Init

    const tmp_window_width: u32 = screen_dimensions.w;
    const tmp_window_height: u32 = screen_dimensions.h;

    const main_window: *sdl.SDL_Window = sdl.SDL_CreateWindow("pixdraw",sdl.SDL_WINDOWPOS_UNDEFINED_DISPLAY(0), sdl.SDL_WINDOWPOS_UNDEFINED_DISPLAY(0), @intCast(tmp_window_width), @intCast(tmp_window_height), sdl.SDL_WINDOW_SHOWN | sdl.SDL_WINDOW_RESIZABLE) orelse return Errors.FailedToCreateWindow;

    const main_renderer: *sdl.SDL_Renderer = sdl.SDL_CreateRenderer(main_window, -1, sdl.SDL_RENDERER_ACCELERATED) orelse return Errors.FailedToCreateRenderer;
    // ^ see https://wiki.libsdl.org/SDL2/SDL_RendererFlags for more info about SDL_CreateRenderer()
    _ = sdl.SDL_SetRenderDrawBlendMode(main_renderer, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (1/2)

    // ~~~~ INIT LAYERS ~~~~
    var layers = std.ArrayList(Picture).init(allocator);
    defer layers.deinit();

    var curr_layer_nb: u8 = 1;
    var nb_layers: u8 = 1;

    var textures = std.ArrayList(*sdl.SDL_Texture).init(allocator);
    defer textures.deinit();

    // ~~~~ INIT MAIN_CANVAS AND MAIN_TEXTURE ~~~~
    const canvas_dimensions = try loadConfigFile(allocator, "pixdraw_config.ini");
    var starting_canvas_width = canvas_dimensions.w;
    var starting_canvas_height = canvas_dimensions.h;

    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);
    // FIXME : reimplement "-i" flag : show infos about file provided on command line

    if (args.len == 1) {
        print("running without parameters \n", .{});

        var new_canvas = Picture.init(allocator, starting_canvas_width, starting_canvas_height, try allocator.alloc(Pixel, starting_canvas_width * starting_canvas_height), Colors.white.toHex());
        new_canvas.fillBgCol();
        try layers.append(new_canvas);

        const tex: *sdl.SDL_Texture = sdl.SDL_CreateTexture(main_renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, @intCast(starting_canvas_width), @intCast(starting_canvas_height)) orelse return Errors.FailedToCreateTexture;
        _ = sdl.SDL_SetTextureBlendMode(tex, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)
        try textures.append(tex);
    } else if (args.len == 2) {
        const ppm_with_header = try readPpmFile(allocator, args[1]);
        defer allocator.free(ppm_with_header);
        const pixels_from_ppm = try getPixelsFromPpm(allocator, ppm_with_header, &starting_canvas_width, &starting_canvas_height);
        // FIXME: remove try and manually create a blank layer on error

        const new_canvas = Picture.init(allocator, starting_canvas_width, starting_canvas_height, pixels_from_ppm, Colors.white.toHex());
        try layers.append(new_canvas);

        const tex: *sdl.SDL_Texture = sdl.SDL_CreateTexture(main_renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, @intCast(starting_canvas_width), @intCast(starting_canvas_height)) orelse return Errors.FailedToCreateTexture;
        _ = sdl.SDL_SetTextureBlendMode(tex, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)
        try textures.append(tex);
    } else {
        print("opening multiple files...\n", .{});
        var i: u16 = 1;

        while (i < args.len) : (i += 1) {
            const ppm_with_header = try readPpmFile(allocator, args[i]);
            defer allocator.free(ppm_with_header);

            const pixels = try getPixelsFromPpm(allocator, ppm_with_header, &starting_canvas_width, &starting_canvas_height);
            // FIXME: remove try and manually create a blank layer on error
            // FIXME : badmagic memoryleak

            const new_canvas = Picture.init(allocator, starting_canvas_width, starting_canvas_height, pixels, Colors.transparent);

            if (i != 1) {
                new_canvas.paintAllSameColorPixels(Colors.white.toHex(), Colors.transparent);
                //FIXME : PPM image format doesnt have the notion of background color. So here, we are forced to assume that white is the background color.
            }

            try layers.append(new_canvas);

            const tex: *sdl.SDL_Texture = sdl.SDL_CreateTexture(main_renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, @intCast(starting_canvas_width), @intCast(starting_canvas_height)) orelse return Errors.FailedToCreateTexture;
            errdefer sdl.SDL_DestroyTexture(textures.items[i]); // <-- good ?

            _ = sdl.SDL_SetTextureBlendMode(tex, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)
            try textures.append(tex);

            nb_layers += 1;
        }
    }

    // ~~~~ BRUSH PREVIEW SETUP ~~~~
    var brush_preview_canvas: Picture = undefined;
    defer brush_preview_canvas.deinit();

    var brush_preview_texture: *sdl.SDL_Texture = undefined;

    brush_preview_canvas = Picture.init(allocator, starting_canvas_width, starting_canvas_height, try allocator.alloc(Pixel, starting_canvas_width * starting_canvas_height), Colors.transparent);
    brush_preview_canvas.fillBgCol();

    brush_preview_texture = sdl.SDL_CreateTexture(main_renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, @intCast(starting_canvas_width), @intCast(starting_canvas_height)) orelse return Errors.FailedToCreateTexture;
    _ = sdl.SDL_SetTextureBlendMode(brush_preview_texture, sdl.SDL_BLENDMODE_BLEND);

    // ~~~~ CONTROL FLOW BOOLEANS ~~~~
    var quit = false;
    var mouse_left_click = false;
    var right_control_key_pressed = false;
    var refresh_asked = true;

    // ~~~~ MAIN LOOP VARIABLES ~~~~
    var frame_start: u32 = 0;
    var frame_end: u32 = 0;
    var frame_count: u32 = 0;

    var paint_color: Pixel = Colors.black.toHex();

    var mode = Mode.paint;

    var brush_size: u16 = 1;

    const shift_factor: u16 = 1;

    // ~~~~ DRAW LINE VARIABLES ~~~~
    var tmpx: i32 = 0;
    var tmpy: i32 = 0;

    var oldtmpx: i32 = 0;
    var oldtmpy: i32 = 0;

    var p1_x: ?i32 = null;
    var p1_y: ?i32 = null;

    // ~~~~ TRIANGLE VARIABLES ~~~~
    var p2_x: ?i32 = null;
    var p2_y: ?i32 = null;

    var p3_x: ?i32 = null;
    var p3_y: ?i32 = null;

    var p4_x: ?i32 = null;
    var p4_y: ?i32 = null;

    // ~~~~ PANNING/ZOOM VARIABLES ~~~~
    var zoom: u32 = 1;
    var xoffset: i32 = 0;
    var yoffset: i32 = 0;

    // ~~~~ UNDOS ~~~~
    var undos_stack = std.ArrayList(Picture).init(allocator);
    defer undos_stack.deinit();

    // ~~~~ BLANK TEX ~~~~
    const palette_tex_width = 200;
    const palette_tex_height = 120;

    const palette_tex: *sdl.SDL_Texture = sdl.SDL_CreateTexture(main_renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, palette_tex_width, palette_tex_height) orelse return Errors.FailedToCreateTexture;
    defer sdl.SDL_DestroyTexture(palette_tex);
    _ = sdl.SDL_SetTextureBlendMode(palette_tex, sdl.SDL_BLENDMODE_BLEND); // <- enable transparency (2/2)

    // ~~~~ PALETTE STUFF ~~~~
    var palette_pixels: [palette_tex_width * palette_tex_height]Pixel = undefined;
    var palette = Picture.init(allocator, palette_tex_width, palette_tex_height, palette_pixels[0..], Colors.white.toHex());
    // stack allocated, no need to free

    palette.fillBgCol();
    generatePalette(palette);

    // ~~~~ JUST IN CASE ~~~~
    _ = sdl.SDL_RenderSetLogicalSize(main_renderer, @intCast(starting_canvas_width), @intCast(starting_canvas_height));
    // should we manually handle screen res management ?

    // ~~~~ MAIN LOOP ~~~~
    while (!quit) {
        frame_count = sdl.SDL_GetTicks(); // <- Get the number of milliseconds since SDL library initialization.
        frame_start = frame_count;

        var event: sdl.SDL_Event = undefined;
        while (sdl.SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                sdl.SDL_WINDOWEVENT => {
                    switch (event.window.event) {
                        sdl.SDL_WINDOWEVENT_RESIZED => {
                            _ = sdl.SDL_RenderSetLogicalSize(main_renderer, @intCast(layers.items[0].width), @intCast(layers.items[0].height));
                            // should we manually handle screen res management ?
                        },

                        sdl.SDL_WINDOWEVENT_CLOSE => {
                            // user pressed the cross button on main_window
                            while (undos_stack.items.len > 0) {
                                const tmp = undos_stack.pop();
                                tmp.deinit();
                            }
                        },
                        else => {},
                    }
                },

                sdl.SDL_QUIT => {
                    while (undos_stack.items.len > 0) {
                        const tmp = undos_stack.pop();
                        tmp.deinit();
                    }
                    quit = true;
                },

                sdl.SDL_KEYDOWN => {
                    switch (event.key.keysym.sym) {
                        sdl.SDLK_RCTRL, sdl.SDLK_LCTRL => {
                            right_control_key_pressed = true;
                        },
                        else => {},
                    }
                    //https://wiki.libsdl.org/SDL2/SDL_Keycode

                    switch (event.key.keysym.sym) {
                        sdl.SDLK_ESCAPE => {
                            if (!mouse_left_click) {
                                while (undos_stack.items.len > 0) {
                                    const tmp = undos_stack.pop();
                                    tmp.deinit();
                                }
                                quit = true;
                            }
                        },

                        sdl.SDLK_BACKSPACE => {},

                        sdl.SDLK_SPACE => {},

                        sdl.SDLK_RETURN => {},

                        sdl.SDLK_a => {
                            mode = Mode.palette;
                            refresh_asked = true;
                        },

                        sdl.SDLK_b => {
                            mode = Mode.all_same_color;
                        },

                        sdl.SDLK_c => {
                            mode = Mode.circle;
                        },

                        sdl.SDLK_d => {
                            mode = Mode.color_pick;
                        },

                        sdl.SDLK_e => {
                            mode = Mode.draw_line;
                        },

                        sdl.SDLK_f => {
                            mode = Mode.bucket;
                        },

                        sdl.SDLK_g => {},

                        //FIXME : implement turbo canvas shifting
                        sdl.SDLK_h => {},

                        sdl.SDLK_i => {},

                        sdl.SDLK_j => {},

                        sdl.SDLK_k => {},

                        sdl.SDLK_l => {},

                        sdl.SDLK_m => {

                            // ~~~~ add layer to undos stack ~~~~
                            const tmp_canvas = try layers.items[curr_layer_nb - 1].clone();

                            try undos_stack.append(tmp_canvas);

                            if (undos_stack.items.len >= max_undos) {
                                const first_item = undos_stack.orderedRemove(0);
                                first_item.deinit();
                            }
                            // ~~~~~~~~~

                            try layers.items[curr_layer_nb - 1].flip();

                            refresh_asked = true;
                        },

                        sdl.SDLK_n => {
                            mode = Mode.paint;
                        },

                        sdl.SDLK_o => {
                            if (nb_layers == 1) {
                                print("there is only 1 layer. Not destroying it\n", .{});
                            } else {
                                var top_layer = layers.pop();
                                top_layer.deinit();

                                const top_texture = textures.pop();
                                sdl.SDL_DestroyTexture(top_texture);

                                nb_layers -= 1;
                            }
                        },

                        sdl.SDLK_p => {
                            const w = layers.items[curr_layer_nb - 1].width;
                            const h = layers.items[curr_layer_nb - 1].height;
                            var new_layer = Picture.init(allocator, w, h, try allocator.alloc(Pixel, w * h), Colors.transparent);

                            new_layer.fillBgCol();

                            const new_tex = sdl.SDL_CreateTexture(main_renderer, sdl.SDL_PIXELFORMAT_ARGB8888, sdl.SDL_TEXTUREACCESS_STREAMING, @intCast(new_layer.width), @intCast(new_layer.height)) orelse return Errors.FailedToCreateTexture;
                            _ = sdl.SDL_SetTextureBlendMode(new_tex, sdl.SDL_BLENDMODE_BLEND);

                            try layers.append(new_layer);
                            try textures.append(new_tex);

                            nb_layers += 1;

                            print("creating 1 new layer. Number of layers = {d} \n", .{nb_layers});
                        },

                        sdl.SDLK_q => {
                            if (!mouse_left_click) {
                                while (undos_stack.items.len > 0) {
                                    const tmp = undos_stack.pop();
                                    tmp.deinit();
                                }
                                quit = true;
                            }
                        },

                        sdl.SDLK_r => {
                            mode = Mode.bezier; 
                        },

                        sdl.SDLK_s => {
                            var temp_name_buffer: [80]u8 = undefined;
                            const output_filename = try std.fmt.bufPrint(temp_name_buffer[0..], "{s}{d}{s}", .{ "px_", std.time.milliTimestamp(), ".ppm" });
                            // ^ NOTE : we generate a random name for output file to prevent overwritting an existing file
                            // and because its a pain to ask user to enter name

                            if (right_control_key_pressed) {
                                const exported = try mergePictureStack(allocator, layers);
                                try exported.saveAsPpm(output_filename);
                                exported.deinit();
                            } else {
                                const curr_layer = layers.items[curr_layer_nb - 1];
                                try curr_layer.saveAsPpm(output_filename);
                            }
                        },

                        sdl.SDLK_t => {
                            mode = Mode.triangle;
                        },

                        sdl.SDLK_u => {
                            mode = Mode.quad;
                        },

                        sdl.SDLK_v => {},

                        sdl.SDLK_w => {},

                        sdl.SDLK_x => {},

                        sdl.SDLK_y => {
                            try layers.items[curr_layer_nb - 1].flop();
                            refresh_asked = true;
                        },

                        sdl.SDLK_z => {
                            if (undos_stack.items.len > 0) {
                                const tmp = undos_stack.pop();
                                tmp.copyTo(layers.items[curr_layer_nb - 1]);
                                tmp.deinit();
                                refresh_asked = true;
                            } else {}
                        },

                        // ~~~~ NUMPAD ~~~~
                        sdl.SDLK_KP_PLUS => {
                            zoom *= 2;

                            xoffset = 0;
                            yoffset = 0;

                            print("zoom : {d}\n", .{zoom});
                            refresh_asked = true;
                        },

                        sdl.SDLK_KP_MINUS => {
                            if (zoom != 1) {
                                zoom = zoom / 2;
                            }
                            xoffset = 0;
                            yoffset = 0;

                            print("zoom : {d}\n", .{zoom});
                            refresh_asked = true;
                        },

                        sdl.SDLK_1 => {
                            brush_size += 1;
                            print("brush size increased to : {d}\n", .{brush_size});
                        },

                        sdl.SDLK_2 => {
                            if (brush_size > 1) {
                                brush_size -= 1;
                                print("brush size decreased to : {d}\n", .{brush_size});
                            }
                        },

                        sdl.SDLK_3 => {},

                        sdl.SDLK_4 => {},

                        sdl.SDLK_5 => {},

                        sdl.SDLK_6 => {
                            showHelp();
                        },

                        sdl.SDLK_7 => {

                            // ~~~~ add layer to undos stack ~~~~
                            const undo_stack = try layers.items[curr_layer_nb - 1].clone();
                            try undos_stack.append(undo_stack);

                            if (undos_stack.items.len >= max_undos) {
                                const first_item = undos_stack.orderedRemove(0);
                                first_item.deinit();
                            }
                            // ~~~~~~~~~

                            try layers.items[curr_layer_nb - 1].shiftLeft(shift_factor);
                            refresh_asked = true;
                        },

                        sdl.SDLK_8 => {

                            // ~~~~ add layer to undos stack ~~~~
                            const undo_stack = try layers.items[curr_layer_nb - 1].clone();
                            try undos_stack.append(undo_stack);

                            if (undos_stack.items.len >= max_undos) {
                                const first_item = undos_stack.orderedRemove(0);
                                first_item.deinit();
                            }
                            // ~~~~~~~~~

                            try layers.items[curr_layer_nb - 1].shiftDown(shift_factor);
                            refresh_asked = true;
                        },

                        sdl.SDLK_9 => {
                            // ~~~~ add layer to undos stack ~~~~
                            const undo_stack = try layers.items[curr_layer_nb - 1].clone();
                            try undos_stack.append(undo_stack);

                            if (undos_stack.items.len >= max_undos) {
                                const first_item = undos_stack.orderedRemove(0);
                                first_item.deinit();
                            }
                            // ~~~~~~~~~

                            try layers.items[curr_layer_nb - 1].shiftUp(shift_factor);
                            refresh_asked = true;
                        },

                        sdl.SDLK_0 => {

                            // ~~~~ add layer to undos stack ~~~~
                            const undo_stack = try layers.items[curr_layer_nb - 1].clone();
                            try undos_stack.append(undo_stack);

                            if (undos_stack.items.len >= max_undos) {
                                const first_item = undos_stack.orderedRemove(0);
                                first_item.deinit();
                            }
                            // ~~~~~~~~~

                            try layers.items[curr_layer_nb - 1].shiftRight(shift_factor);
                            refresh_asked = true;
                        },

                        // ~~~~ ARROW KEYS ~~~~
                        sdl.SDLK_UP => {
                            if (zoom != 1 and yoffset > 0) {
                                yoffset -= 1;
                                refresh_asked = true;
                            }
                        },

                        sdl.SDLK_DOWN => {
                            const panning_limit = ((zoom - 1) * starting_canvas_height) / zoom;

                            if (zoom != 1 and yoffset < panning_limit) {
                                yoffset += 1;
                                refresh_asked = true;
                            }
                        },

                        sdl.SDLK_LEFT => {
                            if (zoom != 1 and xoffset > 0) {
                                xoffset -= 1;
                                refresh_asked = true;
                            }
                        },

                        sdl.SDLK_RIGHT => {
                            const panning_limit = ((zoom - 1) * starting_canvas_width) / zoom;
                            if (zoom != 1 and xoffset < panning_limit) {
                                xoffset += 1;
                                refresh_asked = true;
                            }
                        },

                        // ~~~~ FUNCTION KEYS ~~~~
                        sdl.SDLK_F2 => {
                            if (curr_layer_nb == nb_layers) {
                                print("already on top layer \n", .{});
                            } else {
                                curr_layer_nb += 1;
                                print("we are now on layer {d} \n", .{curr_layer_nb});
                            }
                        },

                        sdl.SDLK_F3 => {
                            if (curr_layer_nb == 1) {
                                print("already on bottom layer \n", .{});
                            } else {
                                curr_layer_nb -= 1;
                                print("we are now on layer {d} \n", .{curr_layer_nb});
                            }
                        },
                        else => {},
                    }
                },

                sdl.SDL_KEYUP => { // key is released

                    switch (event.key.keysym.sym) {
                        sdl.SDLK_RCTRL, sdl.SDLK_LCTRL => {
                            right_control_key_pressed = false;
                        },
                        else => {},
                    }

                    switch (event.key.keysym.sym) {
                        sdl.SDLK_RIGHT => {},

                        sdl.SDLK_LEFT => {},

                        sdl.SDLK_UP => {},

                        sdl.SDLK_DOWN => {},

                        else => {},
                    }
                },

                sdl.SDL_MOUSEWHEEL => {
                    var mousex: i32 = undefined;
                    var mousey: i32 = undefined;
                    _ = sdl.SDL_GetMouseState(&mousex, &mousey);

                    if (event.wheel.y > 0) {} else if (event.wheel.y < 0) {} // scroll down
                },

                sdl.SDL_MOUSEBUTTONUP => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {
                        mouse_left_click = false;
                    }
                },

                sdl.SDL_MOUSEBUTTONDOWN => {
                    if (event.button.button == sdl.SDL_BUTTON_LEFT) {
                        mouse_left_click = true;

                        const x1 = event.motion.x;
                        const y1 = event.motion.y;

                        const z: i32 = @intCast(zoom);
                        const x_adjust: i32 = @divFloor(x1, z) + xoffset;
                        const y_adjust: i32 = @divFloor(y1, z) + yoffset;

                        // ~~~~ add layer to undos stack ~~~~
                        const undo_stack = try layers.items[curr_layer_nb - 1].clone();
                        try undos_stack.append(undo_stack);

                        if (undos_stack.items.len >= max_undos) {
                            const first_item = undos_stack.orderedRemove(0);
                            first_item.deinit();
                        }
                        // ~~~~~~~~

                        if (x_adjust < 0 or y_adjust < 0) {
                            break;
                        }
                        if (x_adjust >= layers.items[curr_layer_nb - 1].width or y_adjust >= layers.items[curr_layer_nb - 1].height) {
                            break;
                        }

                        switch (mode) {
                            Mode.paint => {
                                layers.items[curr_layer_nb - 1].brushPaint(x_adjust, y_adjust, brush_size, paint_color);
                            },
                            Mode.circle => {
                                layers.items[curr_layer_nb - 1].drawCircle(x_adjust, y_adjust, brush_size, paint_color);
                            },
                            Mode.bucket => {
                                try layers.items[curr_layer_nb - 1].bucketFill(x_adjust, y_adjust, paint_color);
                            },
                            Mode.all_same_color => {
                                const casted_x_adj: u64 = @intCast(x_adjust);
                                const casted_y_adj: u64 = @intCast(y_adjust);

                                const index: u64 = @intCast(casted_x_adj + casted_y_adj * @as(u64, layers.items[curr_layer_nb - 1].width));
                                const old_color = layers.items[curr_layer_nb - 1].pixels[index];

                                layers.items[curr_layer_nb - 1].paintAllSameColorPixels(old_color, paint_color);
                                mode = Mode.paint;
                            },
                            Mode.color_pick => {
                                const casted_x_adj: u64 = @intCast(x_adjust);
                                const casted_y_adj: u64 = @intCast(y_adjust);

                                const index: u64 = @intCast(casted_x_adj + casted_y_adj * @as(u64, layers.items[curr_layer_nb - 1].width));
                                paint_color = layers.items[curr_layer_nb - 1].pixels[index];
                                mode = Mode.paint;
                            },
                            Mode.draw_line => {
                                if (p1_x) |p1x| {
                                    layers.items[curr_layer_nb - 1].drawLine(p1x, p1_y.?, x_adjust, y_adjust, brush_size, paint_color);
                                    p1_x = null;
                                    p1_y = null;
                                } else {
                                    p1_x = x_adjust;
                                    p1_y = y_adjust;
                                }
                            },
                            Mode.triangle => {
                                if  (p1_x == null){
                                    p1_x = x_adjust;
                                    p1_y = y_adjust;
                                } else if(p2_x == null) {
                                    p2_x = x_adjust; 
                                    p2_y = y_adjust;
                                } else if(p3_x == null) {
                                    p3_x = x_adjust;
                                    p3_y = y_adjust;
                                    layers.items[curr_layer_nb - 1].drawTriangle(p1_x.?, p1_y.?, p2_x.?, p2_y.?, p3_x.?, p3_y.?, brush_size, paint_color);

                                    p1_x, p1_y, p2_x, p2_y, p3_x, p3_y = .{null, null, null, null, null, null};
                                } else { unreachable;}
                            },
                            Mode.quad => {
                                if  (p1_x == null){
                                    p1_x = x_adjust;
                                    p1_y = y_adjust;
                                } else if(p2_x == null) {
                                    p2_x = x_adjust; 
                                    p2_y = y_adjust;
                                } else if(p3_x == null) {
                                    p3_x = x_adjust;
                                    p3_y = y_adjust;
                                } else if(p4_x == null) {
                                    p4_x = x_adjust;
                                    p4_y = y_adjust;
                                    layers.items[curr_layer_nb - 1].drawQuad(p1_x.?, p1_y.?, p2_x.?, p2_y.?, p4_x.?, p4_y.?, p3_x.?, p3_y.?, brush_size, paint_color);
                                    p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y = .{null, null, null, null, null, null, null, null};
                                } else {unreachable;}

                            },

                            Mode.palette => {
                                // convert mouse coordinates to new coordinates space

                                const casted_x: u32 = @intCast(event.motion.x);
                                const casted_y: u32 = @intCast(event.motion.y);

                                const x_palette = casted_x * palette_tex_width / starting_canvas_width;
                                const y_palette = casted_y * palette_tex_height / starting_canvas_height;

                                const index: u64 = x_palette + y_palette * palette.width;
                                paint_color = palette.pixels[index];
                                mode = Mode.paint;
                            },

                            Mode.bezier => {
                                if  (p1_x == null){
                                    p1_x = x_adjust;
                                    p1_y = y_adjust;
                                } else if(p2_x == null) {
                                    p2_x = x_adjust; 
                                    p2_y = y_adjust;
                                } else if(p3_x == null) {
                                    p3_x = x_adjust;
                                    p3_y = y_adjust;
                                    layers.items[curr_layer_nb - 1].drawBezier(p1_x.?, p1_y.?, p2_x.?, p2_y.?, p3_x.?, p3_y.?, brush_size, paint_color);
                                    p1_x, p1_y, p2_x, p2_y, p3_x, p3_y = .{null, null, null, null, null, null};
                                } else {unreachable;}
                            },
                        }
                    }
                    refresh_asked = true;
                },

                sdl.SDL_MOUSEMOTION => {
                    oldtmpx = tmpx;
                    oldtmpy = tmpy;

                    tmpx = event.motion.x;
                    tmpy = event.motion.y;

                    const casted_zoom: i32 = @intCast(zoom);

                    const oldtmpx_adjust = @divFloor(oldtmpx, casted_zoom) + xoffset;
                    const oldtmpy_adjust = @divFloor(oldtmpy, casted_zoom) + yoffset;

                    const tmpx_adjust = @divFloor(tmpx, casted_zoom) + xoffset;
                    const tmpy_adjust = @divFloor(tmpy, casted_zoom) + yoffset;

                    // NOTE : canvas is slightly smaller than window. These 2 ifs protect us from pixel overflow
                    if (tmpx_adjust < 0 or tmpy_adjust < 0 or oldtmpx_adjust < 0 or oldtmpy_adjust < 0) {
                        break;
                    }

                    if ((tmpx_adjust >= layers.items[curr_layer_nb - 1].width or tmpy_adjust >= layers.items[curr_layer_nb - 1].height) or
                        (oldtmpx_adjust >= layers.items[curr_layer_nb - 1].width or oldtmpy_adjust >= layers.items[curr_layer_nb - 1].height))
                    {
                        break;
                    }

                    if (mouse_left_click) {
                        refresh_asked = true;

                        brush_preview_canvas.fill(Colors.transparent);
                        brush_preview_canvas.brushPaint(tmpx_adjust, tmpy_adjust, brush_size, Colors.brush_preview_color);

                        switch (mode) {
                            Mode.paint => {
                                layers.items[curr_layer_nb - 1].drawLine(oldtmpx_adjust, oldtmpy_adjust, tmpx_adjust, tmpy_adjust, brush_size, paint_color);
                            },
                            Mode.circle => {},
                            Mode.bucket => {
                                try layers.items[curr_layer_nb - 1].bucketFill(tmpx_adjust, tmpy_adjust, paint_color);
                            },
                            Mode.all_same_color => {},
                            Mode.color_pick => {},
                            Mode.draw_line => {},
                            Mode.triangle => {},
                            Mode.quad => {},
                            Mode.palette => {},
                            Mode.bezier => {},
                        }
                    } else {
                        brush_preview_canvas.fill(Colors.transparent);
                        brush_preview_canvas.brushPaint(tmpx_adjust, tmpy_adjust, brush_size, Colors.brush_preview_color);
                    }
                },
                else => {},
            }
        }

        // ~~~~ REFRESH TEXTURES ~~~~
        if (refresh_asked) {
            if (mode == Mode.palette) {
                var buffer_screen = Picture.init(allocator, palette_tex_width, palette_tex_height, try allocator.alloc(Pixel, palette_tex_width * palette_tex_height), Colors.white.toHex());
                defer buffer_screen.deinit();

                palette.copyTo(buffer_screen);
                _ = sdl.SDL_UpdateTexture(palette_tex, null, @ptrCast(buffer_screen.pixels), @intCast(buffer_screen.width * @sizeOf(Pixel)));
            } else {
                // FIXME : only update all layers when modifying zoom
                // right now, we update all layers at every refresh
                if (zoom == 1) {
                    for (layers.items, 0..) |_, i| {
                        _ = sdl.SDL_UpdateTexture(textures.items[i], null, @ptrCast(layers.items[i].pixels), @intCast(layers.items[i].width * @sizeOf(Pixel)));
                    }
                } else {
                    for (layers.items, 0..) |_, i| {
                        const curr_layer = layers.items[i];
                        const curr_texture = textures.items[i];

                        var buf_canvas = Picture.init(allocator, curr_layer.width, curr_layer.height, try allocator.alloc(Pixel, curr_layer.width * curr_layer.height), curr_layer.bg_col);

                        const reduced = try curr_layer.reduce(curr_layer.width / zoom, curr_layer.height / zoom, @intCast(xoffset), @intCast(yoffset));
                        const doubled = try reduced.scale(curr_layer.width, curr_layer.height);

                        doubled.copyTo(buf_canvas);

                        doubled.deinit();
                        reduced.deinit();

                        _ = sdl.SDL_UpdateTexture(curr_texture, null, @ptrCast(buf_canvas.pixels), @intCast(buf_canvas.width * @sizeOf(Pixel)));

                        buf_canvas.deinit();
                    }
                }
            }
        }

        if (mode != Mode.palette) {
            // NOTE : also apply transformations to brush_preview_canvas. Else, it's hell
            var preview_canvas_buff = Picture.init(allocator, brush_preview_canvas.width, brush_preview_canvas.height, try allocator.alloc(Pixel, brush_preview_canvas.width * brush_preview_canvas.height), brush_preview_canvas.bg_col);

            const reduced = try brush_preview_canvas.reduce(brush_preview_canvas.width / zoom, brush_preview_canvas.height / zoom, @intCast(xoffset), @intCast(yoffset));
            const doubled = try reduced.scale(brush_preview_canvas.width, brush_preview_canvas.height);
            doubled.copyTo(preview_canvas_buff);

            _ = sdl.SDL_UpdateTexture(brush_preview_texture, null, @ptrCast(preview_canvas_buff.pixels), @intCast(preview_canvas_buff.width * @sizeOf(Pixel)));
            preview_canvas_buff.deinit();

            doubled.deinit();
            reduced.deinit();
        }

        // ~~~~ RENDERING ~~~~
        // NOTE : the rule is a single renderer for multiple textures
        // NOTE : we cant create another renderer for same window
        _ = sdl.SDL_RenderClear(main_renderer);

        for (textures.items, 0..) |_, i| {
            _ = sdl.SDL_RenderCopy(main_renderer, textures.items[i], null, null);
        }

        if (mode == Mode.palette) {
            _ = sdl.SDL_RenderCopy(main_renderer, palette_tex, null, null);
            //NOTE : very important
            //updated when we draw palette on screen
        } else {
            // dont show brush preview texture in palette mode
            _ = sdl.SDL_RenderCopy(main_renderer, brush_preview_texture, null, null);
        }

        refresh_asked = false;

        _ = sdl.SDL_RenderPresent(main_renderer);
        // ^ Update the screen with any rendering performed since previous call.
        // NOTE : order is always renderClear() -> renderCopy() -> renderPresent()

        // ~~~~ REGULATE fps ~~~~
        frame_end = sdl.SDL_GetTicks();
        const elapsed_time_in_loop_iteration = frame_end - frame_start;
        const time_wanted_for_1_loop_iteration = 1000 / fps;

        if (elapsed_time_in_loop_iteration < time_wanted_for_1_loop_iteration) {
            sdl.SDL_Delay(time_wanted_for_1_loop_iteration - elapsed_time_in_loop_iteration);
        }
    }

    // ~~~~ CLEANUP BEFORE EXITING ~~~~

    sdl.SDL_DestroyTexture(palette_tex);

    for (layers.items, 0..) |_, i| {
        layers.items[i].deinit();
    }

    for (textures.items, 0..) |_, i| {
        sdl.SDL_DestroyTexture(textures.items[i]);
    }

    sdl.SDL_DestroyTexture(brush_preview_texture);

    sdl.SDL_DestroyRenderer(main_renderer);
    sdl.SDL_DestroyWindow(main_window);
    sdl.SDL_Quit();

    showPointer();
}

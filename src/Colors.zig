const Pixel = u32;

pub const Rgb = struct {
    const Self = @This();
    r: u8,
    g: u8,
    b: u8,

    pub fn toHex(self: Self) Pixel {
        return (0xFF000000 + @as(u32, self.r) * 65536 + @as(u32, self.g) * 256 + @as(u32, self.b));
    }
};

pub const white = Rgb{ .r = 255, .g = 255, .b = 255 };
pub const black = Rgb{ .r = 0, .g = 0, .b = 0 };

pub const red = Rgb{ .r = 255, .g = 0, .b = 0 };
pub const green = Rgb{ .r = 0, .g = 255, .b = 0 };
pub const blue = Rgb{ .r = 0, .g = 0, .b = 255 };

pub const magenta = Rgb{ .r = 255, .g = 0, .b = 255 };
pub const cyan = Rgb{ .r = 0, .g = 255, .b = 255 };
pub const yellow = Rgb{ .r = 255, .g = 255, .b = 0 };

pub const pink = Rgb{ .r = 255, .g = 190, .b = 200 };

pub const transparent = 0x00000000;
pub const brush_preview_color = 0x7F00FF00;

/// ~~~~ HSV ~~~~
pub const Hsv = struct {
    h: u16, //angle, 0 -> 360
    s: f32, // 0 -> 100
    v: f32, // 0 -> 100
};

pub fn rgb2Hsv(rgb: Rgb) Hsv {
    //TODO
    _ = rgb;
    return undefined;
}

pub fn hsv2Rgb(hsv: Hsv) Rgb {
    //TODO
    _ = hsv;
    return undefined;
}

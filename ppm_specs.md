# PPM FILE FORMAT SPECS

[src](https://netpbm.sourceforge.net/doc/ppm.html)
[wikipedia](https://en.wikipedia.org/wiki/Netpbm)
[for ascii stuff](https://www.asciitable.com/)


## example
this is a 3x2 ppm file.
pixels are : black, white, white, red, green, blue


```
00000000: 5036 0a33 2032 2032 3535 0a00 0000 ffff  P6.3 2 255......
00000010: ffff ffff ff00 0000 ff00 0000 ff         .............
```


### header part
```
5036 0a33 2032 2032 3535 0a
```

"P6\nwidth height 255\n"

### pixels part
```
000000 ffffff ffffff ff0000 00ff00 0000ff
-----  ------ ------ ------ ------ ------
black  white  white  red    green  blue
``` 

### some ascii shenanigans

```
dec|hex|char
50 |80 |'P'
54 |36 |'6'
10 |0a |'\n'
32 |20 |' '
```


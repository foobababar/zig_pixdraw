# zig_pixdraw

## datadump

simple pixel art editor
supports ppm file format

linux only

requires sdl2, X11, Xi


compile with :
```
zig build
```

run with (after building) :
```
zig-out/bin/pixdraw
```
or with : 
```
zig-out/bin/pixdraw filename.ppm
```

or with :
```
zig-out/bin/pixdraw -h 
```

## run on wayland 
```
SDL_VIDEODRIVER=wayland zig-out/bin/pixdraw
```

## ubuntu build instruction

on ubuntu 22.04.2
```
sudo apt install libsdl2-2.0-0
sudo apt install libsdl2-dev
download zig binary
add zig to path
git clone https://gitlab.com/foobababar/zig_pixdraw.git
cd zig_pixdraw
zig build run
```

## added appimage to repo

## license 
MIT



# TODO list for pixdraw

## 16/1/23

- [x] export picture on quit
- [x] bigger brush
- [x] brush struct
- [x] pixdraw code cleanup

## 17/1/23

- [x] brush code reformat
- [x] out of bounds bug fixed

## 18/01/23

- [x] better exceptions in main

## 19/01/23

- [x] draw rectangle top left of window (demo purpose)
- [x] bug : pixel appears on other side of canvas when drawing to extreme left

## 20/01/23

- [x] draw pixel immediately when click

## 22/01/23

- [x] 'diamond' brush

## 25/01/23

- [x] movable window
- [x] resizable window

## 26/01/23

- [x] round brush
- [x] eraser

## 27/01/23

- [x] export as filename

## 29/01/23

- [x] brush preview following cursor

## 30/01/23

- [x] open, read and display ppm files

## 3/2/23

- [x] panning first draft

## 7/2/23

### big regression
* no more ppm opening
* no more brush preview
* no more panning

- [x] dissociated window_width and canvas_width 

## 11/2/23
* zoom almost working
- [x] zoom first draft


## 12/2/23
- [x] reset offset when zoom == 1
- [x] resizeable window

## 13/2/23
- [x] cap offset
- [x] offset "accelerator" (using j, k)
- [x] open, display and edit ppm

## 14/2/23
- [x] dynamic window size


## 15/2/23
- [x] true offset accelerator
- [x] manpage, argument at startup

## 19/2/23
- [x] zoom x4 (kinda ?)
- [x] brush
- [x] restore brush preview

## 20/2/23
- [x] fix panning when zoom == 4
- [x] don't allow panning when zoom == 1
- [x] better preview

## 21/2/23
- [x] undo

## 22/2/23
- [x] hide cursor ?
- [x] saveAs() generics

## 24/2/23
- [x] bucket fill
- [x] different colors

## 25/2/23
- [x] canvas scale_up / down

## 26/2/23
- [x] SDL_PollEvent
- [x] fixed zoom perf issues  
- [x] handle fps

## 27/2/23
- [x] color picker

## 28/2/23
- [x] shift canvas by pressing jkli

## 1/3/23
- [x] code cleanup
 

## 2/3/23
- [x] remove all global variables

## 4/3/23
- [x] second layer  

## 5/3/23
- [x] import ppm on second layer

## 7/3/23
- [x] smoother lines 

## 11/3/23
- [x] Canvas datatype
- [x] smoother lines
- [x] build.zig

## 13/3/23
- [x] handle window focus
- [x] rename screens to textures
- [~] toolbox window texture


## 14/3/23
- [x] detect window focus
- [x] second_canvas_enabled
- [x] new tool : fill all pixels of specific color
- [x] some tests


## 17/3/23
- [x] pressure sensitivity first draft using X events


## 18/3/23
- [x] improved get_stylus_pressure
- [x] added some pressure thresholds


## 20/03/23
- [x] more savestates
- [x] savestates v2


## 23/03/23
- [x] better color palette
- [x] detect fullscreen


## [ 24/03/23,...,6/4/23 ]
* mirror canvas vertically
* mirror canvas horizontally
* code improvements
* ubuntu build instructions
* improved screen size detection


## 7/4/23
* do we really need to create a specific file format ? Ppm simplicity is cool too.
* what new features ???



## BUGS

- [] bug when exporting file : size is different ?
- [x] pixel wrapping when going left or right with h/l
- [] canvas size is 880x660, zoom is 2, brush is shifted and drawing is laggy
- [x] brush preview is shifted 560x420
- [] canvas is shifted when zoom == 2 and xoffset/yoffset != 0
- [x] brush preview not showing in zoom==2  : offset/zoom must be applied on preview
- [x] brush preview doesn't scale with glob_brush_size
- [] instant preview update
- [x] fix turbo offset around edges
- [x] crash when opening picture with != dimensions than hardcoded ones
- [x] sometimes, pixel appears on other side of canvas. 
- [] sometimes, mouse_left_click stays to true when going oob
- [x] bug when shift canvas to top
- [?] crash proof get_stylus_pressure
- [x] app doesnt close when closing main window
- [x] fullscreen == out of pixdraw

## TODO

- [] fix canvas rotate
- [] "unhardcode" 40 x 30 for res ?
- [] some "selection tools" ?
- [] rotate canvas 30 degrees counterclock wise
- [] turbo canvas shifting (not implemented yet, but boring)
- [x] command line mode (kinda)
- [] migrate away from sdl ?? (this would be hard)
- [x] better color selection tools
- [] better code in Colors.zig
- [x] layer alternative : lock in place of pixels of specified color
- [] better pressure thresholds
- [] show transparency as color
- [] more tests
- [x] handle save states with function
- [x] only update textures when refresh needed
- [] better lines (again)
- [] handle mouse pointer in toolbox window
- [] layers as array ?
- [x] toolbox on other window
- [] specific file format
- [] picture rescale capabilities
- [] zoom * 8 ??
- [] zoom * 0.5 ??

- [x] binary file on gitlab ?? Appimage
- [] change brush preview color too ??
- [x] thumbnail
- [] colored and beautiful information print on terminal ?
- [x] draw straight line 
- [] config file ; persistent settings

- [?] usable ??

- [] startup screen ?? meh
- [] add other brush forms ?? meh, we can build anything with current brush
- [x] tablet pressure detection
- [] grid ? 
- [x] layers ? meh
